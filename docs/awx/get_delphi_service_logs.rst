Obtenção de log serviço delphi nuvem
====================================

Para obter logs de um serviço rodando na nuvem vamos usar o ``workflow`` "`Obtenção de log serviço delphi nuvem <https://awx.korp.com.br/#/templates/workflow_job_template/318/details>`_"

#. Abra o workflow e clique em ``Launch``

    .. image:: ./images/get_delphi_service_logs_survey.png
        :width: 600

Acessar o AWX e executar o template 'Obtenção de log serviço delphi nuvem'. Será necessário fornecer a seguinte informação:

#. **Serviço**: Escolha o serviço que deseja obter logs

#. Após escolher, clique em next e as logs serão enviadas para o seu email corporativo.

.. note::

    #. Caso as logs não apareçam na caixa de entrada do seu ``email`` ao fim da execução, pode acontecer do email ser considerado spam, por isso, verifique a lixeira e a aba de spam;

