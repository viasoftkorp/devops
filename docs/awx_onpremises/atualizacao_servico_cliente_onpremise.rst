Atualização de serviço em cliente onpremise
===========================================

Esta pagina aborda o passo a passo para a atualização de serviço em cliente onpremise.


#. Acessar os templates do AWX

#. Selecione o ``Workflow Job Template`` "`Atualização de serviço em cliente onpremise <https://awx.korp.com.br/#/templates/workflow_job_template/157/details>`_"

#. Clicar em ``Launch``

#. Selecione os campos:

    - Selecione o Tenant em que será reiniciado o serviço:
    - Digite o nome do serviço: Insira o nome exato do serviço; Para encontrar o nome exato, pode ser usado o workflow: `Visualizar serviços ativos em cliente <get_service_names.html>`_

#. Confirme se o Tenant e nome do serviço estão certos

#. Execute a tarefa

.. note:: 

    Selecione o Tenant e nome do serviço e clique em ``Next``

    .. image:: ./images/atualizacao_servico_cliente_onpremise1.jpg
        :width: 600    

    |

    Confirme as informações e clique em ``Launch``

    .. image:: ./images/atualizacao_servico_cliente_onpremise2.jpg
        :width: 600
