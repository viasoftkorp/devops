Visualizar serviços ativos em cliente
-------------------------------------

Para visualizar serviços ativos, use o ``workflow`` "`Mapeamento de serviços ativos em cliente onpremise <https://awx.korp.com.br/#/templates/workflow_job_template/156/details>`_"

#. Abra o workflow e clique em ``Launch``


#. Será necessário fornecer o **Tenant Id** do cliente;
        .. image:: ./images/map_services1.png
            :width: 600

#. Clique em next, depois em Launch. Depois, clique no ``job`` para visualizar output:
        .. image:: ./images/map_services2.png
            :width: 600

#. Clique na ultima task, após a execução finalizar bem-sucedida:
        .. image:: ./images/map_services3.png
            :width: 600

#. Clique em 'JSON';
        .. image:: ./images/access_json.png
            :width: 600

#. Visualize os logs;
        .. image:: ./images/map_services4.png
            :width: 600
