Configurando BI em clientes nuvem
---------------------------------

Informações fornecidas:

    - Nome da empresa

#. Mapear o ``EnvironmentId`` de todas as bases que serão utilizadas(isso pode ser feito pelo https://portal.korp.com.br).

#. Conectar no banco ``129.151.32.61`` pelo https://pgadmin.korp.com.br/browser/

#. Criar schemas nos bancos ``viasoft_elt``, ``viasoft_vendas_elt`` utilizando a query:

    .. code-block:: sql

        CREATE SCHEMA "<EnvironmentId>"
        AUTHORIZATION postgres;

    Query deve ser rodada nos 2 bancos, uma vez para cada ``EnvironmentId`` do cliente.

#. Criar role cliente com a query:

    .. code-block:: sql

        CREATE ROLE "<usuário>" WITH
            LOGIN
            NOSUPERUSER
            NOCREATEDB
            NOCREATEROLE
            INHERIT
            NOREPLICATION
            CONNECTION LIMIT -1
            PASSWORD '<senha_aleatória>';

    Onde ``<usuário>`` é o nome da empresa concatenado com ``.bi``

    Onde ``<senha_aleatória>`` é uma senha gerada para cada cliente.
    
        - Por exemplo, a empresa 'LECLAIR INDUSTRIA E COMERCIO DE PERFUMES E COSMETICOS LTDA', tem o usuário ``leclair.bi``

#. Alterar permissões do usuário criado nos bancos ``viasoft_elt``, ``viasoft_vendas_elt`` com a query:

    .. code-block:: sql

        GRANT USAGE ON SCHEMA "<EnvironmentId>" TO "<usuário>";
        GRANT SELECT ON ALL TABLES IN SCHEMA "<EnvironmentId>" TO "<usuário>";
        GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA "<EnvironmentId>" TO "<usuário>";

    Query deve ser rodada nos 2 bancos, uma vez para cada ``EnvironmentId`` do cliente.

#. Acessar https://minio.korp.com.br/buckets e criar bucket.

    Nome do bucket deve ser ``datalake-<TenantId>``

#. Solicitar a Área de Serviços realizar a carga de dados (Equipe Murilo).

    - Informar os EnvironmentId

    - Informar TenantId

#. Pedir para Laercio configurar PBIX conforme vídeo `configuração PBIX`_, importando eles do repositório `Viasoft.ELT.BI`_ 

    - Informar os EnvironmentId

#. Compartilhar o Workspace do PowerBI com o cliente `vídeo`_

#. A query de `DRE`_ deve ser executada na base de dados do cliente (base do Korp).

.. _configuração PBIX: https://bitbucket.org/viasoftkorp/devops/src/cfa4a35885421f7e07acc987be9f38b42a4cb205/docs/goglobal/videos/PBIX
.. _Viasoft.ELT.BI: https://bitbucket.org/viasoftkorp/viasoft.elt.bi/src/master/
.. _vídeo: https://korpinformatica.sharepoint.com/:v:/s/PortaldeVideos/EZzaCv1xhqFFsUUOkISQTqsB6faayki6yGcAH5Va7gDXuw?e=MGMIZm
.. _DRE: https://bitbucket.org/viasoftkorp/viasoft.elt.bi/src/master/SQL/DRE_POWER_BI.sql