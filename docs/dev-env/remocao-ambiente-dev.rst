Remoção de Ambiente de Desenvolvedor
------------------------------------

.. note::
    * É possível executar novamente o workflow para o **mesmo e-mail** em caso de falhas.
    * As etapadas já concluidas não serão afetadas e as restantes executadas.
    * Caso o ``ip`` já tenha sido removido ele aparecerá como ``'None'`` nas mensagens de remoção.

.. warning::
    Para remoção o **e-mail é a chave principal**, todo estado criado é associado ao e-mail pelo workflow de Criação de ambiente de desenvolvedor.

Etapas Remoção 
--------------

1. Será validada as informações:
================================

* E-mail do colaborador: ``nome.sobrenome@korp.com.br``
* Deseja executar o setup do desenvolvedor (``Sim`` / ``Não``)

  * Caso ``Sim``: É obrigatório informar o ``Ip Windowns``, será executado full reset e destruida a vm associada ao e-mail.
  * Caso ``Não``: O full reset não será executado e a vm associada ao e-mail será destruida.

.. warning::
    Será exibida uma mensagem para **confirmação**:

    "Será destruida a vm ``dev-nome.sobrenome``, dns: [ ``nome-sobrenome.com``, ``nome-sobrenome-api.com``, ``nome-sobrenome-cdn.com`` ] e liberado o ip: ``192.168.x.xx`` ou ``'None``"

2. Após Aprovação
=================

.. note::
    O workflow irá:
    
   * Executar o playbook do desenvolvedor com a opção **full reset** , caso ``Sim``, caso contrario não será executado.
   * Será removido o ``ip`` alocado para vm **associado ao e-mail**.
   * Será removido os ``dns`` **associado ao e-mail**.
   * Desabilitar o acesso a VPN (existe opção remover).
   * Remover a conta Office 365 **associada ao e-mail**.
   * Remover o acesso ao bitbucket **associado ao e-mail**.
   * Será destruida a vm no proxmox **associado ao e-mail**.
   * Será enviada as alterações para o repositório de gerenciamento de estado das vms.

Etapas que devem ser feitas manuais
-----------------------------------

* remover licença jetbrains

* remover api key antropic OpenRouter
