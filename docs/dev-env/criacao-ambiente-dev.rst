Criação de Ambiente de Desenvolvedor
------------------------------------

.. note::
    * É possível executar novamente o workflow para o **mesmo e-mail** em caso de falhas.
    * As etapas já concluidas não serão afetadas e as restantes executadas.

Informações
============

.. note::
    O workflow irá:

    * Buscar um ``ip`` livre no range ``192.168.1.50`` a ``192.168.1.99``.
    * Gerar um endereço ``mac`` aleatório.
    * Fixar ``ip`` descoberto com mac gerado.
    * Criar os dns com base no e-mail fornecido: ``nome-sobrenome.com``, ``nome-sobrenome-api.com``, ``nome-sobrenome-cdn.com``.
    * Criar acesso a VPN - ``nome.sobrenome``.
    * Criar conta Office365 com email informado.
    * Criar usuário no Portal - ``nome.sobrenome``
    * Criar vm no proxmox com nome ``dev-nome.sobrenome`` e ``mac`` gerado anteriormente.
    * Anviar arquivos de estado e registro de novo host gerados para o repositório git - ``viasoftkorp/iac``
    * Executar o setup de desenvolvedor (opcional)

.. warning::
    O **e-mail** do colaborador será a base para toda lógica de informações/gerenciamento do ambiente. 
    
    Será usado para criar **dns**, **nome da vm**, **diretório para armazenamento de estado** e outras informações da role.

Criando ambiente pelo workflow
==============================

* Informe o e-mail do colaborador: ``nome.sobrenome@korp.com.br``
* Quantidade memória ram ``GB``
* Quantidade cores
* Storage que será usado para montar o volume (``NVME01`` / ``NVME02``)
* Tamanho do volume em ``GB``
* Deseja executar o setup do desenvolvedor (``Sim`` / ``Não``)

  * Caso ``Sim``: É obrigatório os campos: ``Versão`` e ``Ip Windowns``.
  * Caso ``Não``: Será criada apenas a máquina e suas configurações de rede, não é necessário informar ``Versão``, ``Apps`` ou ``Ip Windowns``.

.. note::
    A execução do playbook pode levar alguns minutos, podemos acompanhar os logs e ver eventuais erros; 
    
    Na etapa de execução de playbook já é possível estabelecer uma conexão na máquina provisionada.

* Logs de execução do playbook: ``/home/korp/setup_dev.log``
  
  * Ver/acompanhar logs: ``cat /home/korp/setup_dev.log`` ou ``tail -f /home/korp/setup_dev.log``
