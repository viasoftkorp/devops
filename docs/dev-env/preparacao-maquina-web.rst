Preparação Maquina Web
----------------------

#. Ajustar especificações da Maquina:
    * Memória: 16Gb ou superior;
    * Armazenamento: em NVME, 250Gb ou Superior
    * Processador: I5 ou superior;
    * Wifi: 5G nativo ou receptor 5G USB instalado;
 
#. Configuração Básica do Windows:
    * Instalação do Windows 11
    * Instalar atualizações do SO
    * Atualizar drivers (Driver Booster)
    * Desabilitar IPv6
    * Habilitar RDP
    * Remover bloatware
    * Renomear Maquina (NOME-SOBRENOME)
 
#. Configuração padrão colaboradores Korp
    * Instalação dos programas:

      * Google Chrome
      * 7Zip
      * Adobe Reader
      * Microsoft Teams (Corporativo)
      * Everything
 
        * Desabilitar no startup

      * Microsip

        * Configurar canais de voz
        * Habilitar servidor STUN
        * Configurar Conta

      * LightShot
      * Obs Studio
      * Camtasia
      * Appcontroler

        * Configurar acessos as Bases (Tiago)

      * Zabbix Agent 2

        * Identificar a maquina como web-<Colaborador>
        * Apontar para o servidor Zabbix : zabbix-server-interno.korp.com.br

      * Berqun
    * Instalação de Certificados CA
 
#. Configuração para o Desenvolvimento
    * Rodar o Workflow para criação de ambiente Linux
    * Instalação de certificados CA-dev
    * Instalação dos programas:
 
      * NodeJS
      * Angular
      * .NET SDK 6
      * GoLang 1.19
      * Visual Studio Code
      * Jetbrains Goland
      * Jetbrains Rider
      * SQL Server 2022 - edição Development
  
        * Habilitar protocolo TCP/IP
        * Mixed mode
        * Garantir que firewall está aberto

      * SQL Server Management Studio
      * Notepad++
      * Git
      * TDM GCC
      * FileZilla
      * Python
      * Postman
      * Pgadmin
      * Git tortoise
 
    * Instalação do Korp e Serviços Delphi

      * Configurar Variáveis de Ambiente
      * Instalar Versão mais recente do korp
      * Instalar Serviços Delphi





