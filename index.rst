Contents
--------

.. toctree::
   :maxdepth: 2
   :caption: GoGlobal
   
   docs/goglobal/nova-empresa-nuvem
   docs/goglobal/migracao-empresa-nuvem
   docs/goglobal/versao_servicos
   docs/goglobal/configuracao-bi-nuvem
   docs/goglobal/instalacao-certificado-nf-viasoft

.. toctree::
   :maxdepth: 2
   :caption: Utils
   
   docs/utils/criacao-servico/index   
   docs/utils/servidor-linux/index
   docs/utils/renovacao-certificado/index
   docs/utils/postgres

.. toctree::
   :maxdepth: 5
   :caption: Jenkins
   
   docs/jenkins/index
   docs/jenkins/utils
   docs/jenkins/limit-jenkinsfile-v2

.. toctree::
   :maxdepth: 2
   :caption: Serviços
   
   docs/servicos/index


.. toctree::
   :maxdepth: 2
   :caption: Ambiente Desenvolvedor
   
   docs/dev-env/visao-geral
   docs/dev-env/criacao-ambiente-dev
   docs/dev-env/preparacao-maquina-web
   docs/dev-env/remocao-ambiente-dev
   docs/dev-env/automations.rst
   docs/dev-env/jetbrains-gateway.rst

.. toctree::
   :maxdepth: 2
   :caption: AWX 

   docs/awx/get_delphi_service_logs.rst
   docs/awx/utilizando_o_awx.rst
   docs/awx/sobrescrita_base.rst
   docs/awx/execucao_query.rst
   docs/awx/permissao_dbreader.rst
   docs/awx/liberacao_servico.rst
   docs/awx/atualizacao_servicos_qa_delphi.rst
   docs/awx/atualizacao_build_erp_interno.rst
   docs/awx/atualizaçao_repositorio_versionado_erp.rst

.. toctree::
   :maxdepth: 2
   :caption: AWX On Premises

   docs/awx_onpremises/get_service_logs.rst
   docs/awx_onpremises/get_service_names.rst
   docs/awx_onpremises/automations.rst
   docs/awx_onpremises/get_email_db.rst
   docs/awx_onpremises/atualizacao_servico_cliente_onpremise.rst
   docs/awx_onpremises/acesso_sql_onpremise.rst
